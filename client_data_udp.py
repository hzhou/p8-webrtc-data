import asyncio
import time
import json

from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE


async def consume_signaling(ordenador, data):
    try:
        objeto = RTCSessionDescription(**json.loads(data))
    except Exception as e:
        if data == "ACK":
            try:
                jmensaje = json.dumps({"sdp": ordenador.localDescription.sdp, "type": ordenador.localDescription.type})
                UDPSignaling.transport.sendto(jmensaje.encode())
            except Exception as ack_error:
                print(f"Error sending ACK: {ack_error}")
            return

    if isinstance(objeto, RTCSessionDescription):
        await ordenador.setRemoteDescription(objeto)

        if objeto.type == "answer":
            try:
                await ordenador.setLocalDescription(await ordenador.createAnswer())
                jmensaje = json.dumps({"sdp": ordenador.localDescription.sdp, "type": ordenador.localDescription.type})
                UDPSignaling.transport.sendto(jmensaje.encode())
            except Exception as answer_error:
                print(f"Error sending answer: {answer_error}")

    elif objeto == BYE:
        print("Exiting")
        return


time_start = None


def current_stamp():
    global time_start

    if time_start is None:
        time_start = time.time()
        return 0
    else:
        return int((time.time() - time_start) * 1000000)


async def run_offer(ordenador, signalling):

    channel = ordenador.createDataChannel("chat")
    print(f"channel({channel.label}) > created by local party")

    async def send_pings():
        while True:
            message = f"ping {current_stamp()}"
            print(f"channel({channel.label}) > {message}")
            channel.send(message)
            await asyncio.sleep(1)

    @channel.on("open")
    def on_open():
        asyncio.ensure_future(send_pings())

    @channel.on("message")
    def on_message(message):
        print(f"channel({channel.label}) > {message}")

        if isinstance(message, str) and message.startswith("pong"):
            elapsed_ms = (current_stamp() - int(message[5:])) / 1000
            print(" RTT %.2f ms" % elapsed_ms)

    # send offer
    await pc.setLocalDescription(await pc.createOffer())
    await signalling

    # await consume_signaling(pc, signaling)


class UDPSignaling:
    transport = None
    address = ("127.0.0.1", 9999)

    def __init__(self, message, on_con_lost, ordenador):
        self.pc = ordenador
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        UDPSignaling.transport = transport
        print('Send:', self.message)
        UDPSignaling.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        asyncio.ensure_future(consume_signaling(self.pc, data.decode()))

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def connect(ordenador):
    bucle = asyncio.get_running_loop()
    on_con_lost = bucle.create_future()
    message = "REGISTER CLIENT"

    try:
        transport, protocol = await bucle.create_datagram_endpoint(
            lambda: UDPSignaling(message, on_con_lost, ordenador),
            remote_addr=UDPSignaling.address
        )

        await on_con_lost
    except Exception as e:
        print(f"Error connecting: {e}")
    finally:
        if transport and not transport.is_closing():
            transport.close()
            await transport.wait_closed()


if __name__ == "__main__":

    pc = RTCPeerConnection()
    signaling = connect(pc)
    coro = run_offer(pc, signaling)
    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
        UDPSignaling.transport.close()
        print("Close the socket")
