import asyncio
import json

from aiortc import RTCPeerConnection, RTCSessionDescription
from aiortc.contrib.signaling import BYE


async def consume_signaling(ordenador, data):
    try:
        objeto = RTCSessionDescription(**json.loads(data))
    except json.JSONDecodeError as e:
        print(f"Error decoding JSON: {e}")
        return

    if isinstance(objeto, RTCSessionDescription):
        await ordenador.setRemoteDescription(objeto)

        if objeto.type == "offer":
            try:
                await ordenador.setLocalDescription(await ordenador.createAnswer())
                jmensaje = json.dumps({"sdp": ordenador.localDescription.sdp, "type": ordenador.localDescription.type})
                UDPSignaling.transport.sendto(jmensaje.encode())
            except Exception as offer_error:
                print(f"Error handling offer: {offer_error}")

    elif objeto is BYE:
        print("Exiting")
        return


time_start = None


async def run_answer(ordenador, signalling):

    @ordenador.on("datachannel")
    def on_datachannel(channel):
        print(f"channel({channel.label}) > created by remote party")

        @channel.on("message")
        def on_message(message):

            print(f"channel({channel.label}) > {message}")

            if isinstance(message, str) and message.startswith("ping"):
                message = f"pong{message[4:]}"
                print(f"channel({channel.label}) > {message}")
                channel.send(message)

    await signalling


class UDPSignaling:
    transport = None
    address = ("127.0.0.1", 9999)

    def __init__(self, message, on_con_lost, ordenador):
        self.ordenador = ordenador
        self.message = message
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        UDPSignaling.transport = transport
        print('Send:', self.message)
        UDPSignaling.transport.sendto(self.message.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())
        asyncio.ensure_future(consume_signaling(self.ordenador, data.decode()))

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)

async def connect(ordenador):
    bucle = asyncio.get_running_loop()
    on_con_lost = bucle.create_future()
    message = "REGISTER SERVER"

    try:
        transport, protocol = await bucle.create_datagram_endpoint(
            lambda: UDPSignaling(message, on_con_lost, ordenador),
            remote_addr=UDPSignaling.address
        )

        await on_con_lost
    except Exception as e:
        print(f"Error connecting: {e}")
    finally:
        if transport and not transport.is_closing():
            transport.close()

if __name__ == "__main__":
    pc = RTCPeerConnection()
    signaling = connect(pc)
    coro = run_answer(pc, signaling)

    # run event loop
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(coro)
    except KeyboardInterrupt:
        pass
    finally:
        loop.run_until_complete(pc.close())
