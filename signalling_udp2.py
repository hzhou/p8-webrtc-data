import asyncio
import json

class SignallingProtocol:
    members = {"clientAddress": None, "serverAddress": None}

    def __init__(self):
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport

    def datagram_received(self, data, addr):
        message = data.decode()
        print('Received %r from %s' % (message, addr))

        if message == 'REGISTER CLIENT':
            self.register_client(addr)

        elif message == 'REGISTER SERVER':
            self.register_server(addr)

        else:
            self.handle_json_message(message)

    def register_client(self, addr):
        SignallingProtocol.members["clientAddress"] = addr
        print(f"Client registered at {addr}")

        if SignallingProtocol.members["serverAddress"] is not None:
            self.send_ack(SignallingProtocol.members["serverAddress"])

    def register_server(self, addr):
        SignallingProtocol.members["serverAddress"] = addr
        print(f"Server registered at {addr}")

        if SignallingProtocol.members["clientAddress"] is not None:
            self.send_ack(SignallingProtocol.members["clientAddress"])

    def handle_json_message(self, message):
        try:
            json_message = json.loads(message)
            print("JSON received")
        except ValueError as e:
            print(f"Error decoding JSON: {e}")
            return

        kind = json_message.get('type')
        if kind == 'answer':
            self.send_to_client(message)
        elif kind == 'offer':
            self.send_to_server(message)

    def send_ack(self, destination):
        self.transport.sendto("ACK".encode(), destination)

    def send_to_client(self, message):
        if SignallingProtocol.members["clientAddress"] is not None:
            print(f'Send {message} to {SignallingProtocol.members["clientAddress"]}')
            self.transport.sendto(message.encode(), SignallingProtocol.members["clientAddress"])

    def send_to_server(self, message):
        if SignallingProtocol.members["serverAddress"] is not None:
            print(f'Send {message} to {SignallingProtocol.members["serverAddress"]}')
            self.transport.sendto(message.encode(), SignallingProtocol.members["serverAddress"])

async def main():
    print("Starting UDP server")

    try:
        loop = asyncio.get_running_loop()

        transport, protocol = await loop.create_datagram_endpoint(
            lambda: SignallingProtocol(),
            local_addr=('127.0.0.1', 9999))

        # Wait until the user interrupts the program
        await asyncio.sleep(float('inf'))

    except KeyboardInterrupt:
        print("Server shutting down...")

    finally:
        if transport:
            transport.close()

asyncio.run(main())
